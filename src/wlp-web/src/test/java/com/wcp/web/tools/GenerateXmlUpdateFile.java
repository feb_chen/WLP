package com.wcp.web.tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 製作WCP配置文件的增量升級包
 * 
 * @author macpl
 *
 */
public class GenerateXmlUpdateFile {

	public static void main(String[] args) throws IOException {
		System.out.println("启动");
		List<File> fullXmls = new ArrayList<>();
		String path = "D:\\JavaWorkspace\\workspace\\wcp-trunk\\wcp-web\\src\\main";
		// ---------------------------------------------------------------------
		fullXmls.add(new File(path + "\\resources\\WcpInterConfig.xml"));
		fullXmls.add(new File(path + "\\resources\\WcpWebConfig.xml"));
		File versionXml = new File(path + "\\resources\\config\\xmlChecks.xml");
		String startVersion = "4.1.9";// 增量配置中不含有该版本(如：3.5.a)
		String toVersion = "4.2.2";// 增量配置中含有该版本
		// ----------------------------------------------------------------------
		File ofile = new File("D:\\wcptemp\\config" + startVersion + "To" + toVersion + ".xml");
		// 1.读取完整配置文件(多个)
		System.out.println("加载全部配置文件中...");
		Map<String, Element> elementMaps = new HashMap<>();
		for (File file : fullXmls) {
			Document document = Jsoup.parse(file, "utf-8");
			Elements elements = document.getElementsByTag("parameter");
			for (Element node : elements) {
				elementMaps.put(node.attr("name"), node);
			}
		}
		System.out.println("加载全部配置文件" + elementMaps.size());
		System.out.println("----------------------------");
		List<String> updataNames = new ArrayList<>();// 只包含需要更新的配置项
		List<String> allNames = new ArrayList<>();// 用于校验check项是否包含所有配置项
		// 2.读取配置文件版本列表
		{
			System.out.println("计算从版本(" + startVersion + ")到版本【" + toVersion + "】之间的增量配置文件:");
			Document versionDocument = Jsoup.parse(versionXml, "utf-8");
			Elements elements = versionDocument.getElementsByTag("parameter");
			for (Element node : elements) {
				String versions = node.attr("versions");
				String name = node.text();
				allNames.add(name);
				if (StringUtils.isNotBlank(versions)) {
					if (versions.compareTo(startVersion) > 0) {
						if (versions.compareTo(toVersion) <= 0) {
							System.out.println(versions + ":" + name);
							updataNames.add(name);
						}
					}
				}
			}
			System.out.println("共" + updataNames.size() + "項");
		}
		System.out.println("----------------------------------------");
		checkXMLConfig(elementMaps, allNames);
		System.out.println("校验成功，该check表包含所有配置项:" + allNames.size());
		System.out.println("版本" + startVersion + "升级到" + toVersion + "需要增加项:" + updataNames.size());

		// 3.更具版本差异从完整配置中取出增量配置
		{
			System.out.println("----------------------------------------");
			System.out.println(updataNames.size() + "项写入增量配置文件到" + ofile.getPath());
			String grouInner = "";
			for (String name : updataNames) {
				Element node = elementMaps.get(name);
				if (node == null) {
					throw new RuntimeException("配置項" + name + "沒有存在于完整配置文件中");
				}
				grouInner = grouInner + node.outerHtml() + "\n";
				// System.out.println(node.outerHtml());
			}
			String xmlGroup = "<group describe='" + startVersion + "TO" + toVersion + "增量配置' name='" + startVersion
					+ "TO" + toVersion + "'>\n" + grouInner + "</group>";
			FileWriter writer;
			try {
				if (!ofile.exists()) {
					ofile.createNewFile();
				}
				writer = new FileWriter(ofile);
				writer.write(xmlGroup);
				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void checkXMLConfig(Map<String, Element> elementMaps, List<String> updataNames) {
		// 获得所有配置项
		Set<String> allkey = elementMaps.keySet();
		for (String key : allkey) {
			if (!updataNames.contains(key)) {
				throw new RuntimeException(key + "未在xmlChecks文件中注册!");
			}
		}
		// 检查版本对照表中是否包含所有配置项
	}

}
